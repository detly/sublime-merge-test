/* Test C file for Sublime Merge. */


#ifndef SMC_H
#define SMC_H

#define SMC_TEST (0)

typedef enum {
    smc_first,
    smc_second
} smc_test_enum;

typedef struct {
    int field;
} smc_t;

/* Extra stuff to force context break. */

/* First function. */
void smc_f1(void);

/* Test comment. */
void smc_f2(void);

#endif /* SMC_H */
